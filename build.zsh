#! /bin/zsh

python3 -m venv ./.build-env
source ./.build-env/bin/activate
pip3 install -r requirements.txt
make all
