# Copyright 2021, 2022 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

__version__ = '1.1.1'

import random, hashlib, sys
from PIL import Image, ImageDraw

DEFAULT_SIZE = 200
DEFAULT_PARTITIONS = 5

def init_random(seed_string):
    m = hashlib.blake2b()
    m.update(seed_string.encode("UTF-8"))
    return random.Random(m.digest())

def build_colortable():
    # The colors are the four brightest from
    # https://lospec.com/palette-list/nicole-punk-82
    # Random testing made it seem four colors makes for a useful amount of
    # neighbors creating shapes.
    return ((0xfa, 0xf5, 0xd8),
            (0xd8, 0xae, 0x8b),
            (0xcd, 0x5f, 0x2a),
            (0xf2, 0xab, 0x37))

def draw_circles(draw, size, rng, partitions):
    colors = build_colortable()
    for y in range(partitions):
        for x in range(partitions):
            x0 = size * x
            y0 = size * y
            x1 = size * (x + 1) - 1
            y1 = size * (y + 1) - 1
            draw.ellipse((x0, y0, x1, y1), fill=rng.choice(colors))

def fill_gaps(img, draw, size, partitions):
    for y in range(partitions):
        for x in range(partitions):
            center_first = (size * x + size / 2, size * y + size / 2)
            color_first = img.getpixel(center_first)
            if (x + 1) < partitions:
                center_right = (size * (x + 1) + size / 2, size * y + size / 2)
                color_right = img.getpixel(center_right)
                if color_first == color_right:
                    draw.rectangle((size * x + size / 2, size * y,
                            size * (x + 1) + size / 2 - 1, size * (y + 1) - 1),
                            fill=color_first)
            if (y + 1) < partitions:
                center_below = (size * x + size / 2, size * (y + 1) + size / 2)
                color_below = img.getpixel(center_below)
                if color_first == color_below:
                    draw.rectangle((size * x, size * y + size / 2,
                            size * (x + 1) - 1, size * (y + 1) + size / 2 - 1),
                            fill=color_first)

def draw_avatar(img, rng, total_size, partitions):
    draw = ImageDraw.Draw(img)
    draw.rectangle((0, 0, total_size - 1, total_size -1), fill=(0, 0, 0))
    partition_size = total_size // partitions
    if total_size % partitions != 0:
        print("Warning: Total size isn't a multiple of partitions."
                " Avatar will not be rendered 100% regularly.", file=sys.stderr)
    draw_circles(draw, partition_size, rng, partitions)
    fill_gaps(img, draw, partition_size, partitions)

def generate_avatar(seed_string, output, total_size=DEFAULT_SIZE,
        partitions=DEFAULT_PARTITIONS):
    rng = init_random(seed_string)
    with Image.new("RGB", (total_size, total_size)) as img:
        draw_avatar(img, rng, total_size, partitions)
        img.save(output, 'PNG')
