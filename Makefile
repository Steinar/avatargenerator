SHELL=/bin/zsh

.PHONY: clean dist all

clean:
	find . -name __pycache__ -print0 | xargs -0 -r rm -rf
	find . -name '*.egg-info' -print0 | xargs -0 -r rm -rf
	rm -rf dist

dist:
	python3 setup.py sdist

all: dist
